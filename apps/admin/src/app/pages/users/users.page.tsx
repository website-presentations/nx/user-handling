import { useFindAllUserQuery } from '@user-handling/data-access';
/* COMPONENTS */
import UsersTableComponent from '@/components/users/users-table/users-table.component';
import ProgressBarContainer from '@/components/progress-bar/progress-bar.container';
/* MUI */
import { Container } from '@mui/material';

const UsersPage = (): JSX.Element => {
  const { data: findAllUserQuery, loading } = useFindAllUserQuery({
    variables: { userCriteriaInput: {} },
  });

  if (loading) {
    return <ProgressBarContainer />;
  }

  return (
    <Container maxWidth="sm" sx={{ zIndex: 10 }}>
      <UsersTableComponent users={findAllUserQuery?.findAllUser ?? []} />
    </Container>
  );
};

export default UsersPage;

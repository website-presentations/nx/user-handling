import CommonLoginComponent from '@/components/login/common-login/common-login.component';

const LoginPage = (): JSX.Element => {
  return <CommonLoginComponent />;
};

export default LoginPage;

import {
  useGetAdminCountQuery,
  useGetUserCountQuery,
} from '@user-handling/data-access';
/* MUI */
import { List, Divider, Container, Typography, useTheme } from '@mui/material';
import { GroupRounded, AccountCircle } from '@mui/icons-material';
/* COMPONENTS */
import StaticsItemComponent from '@/components/home/statics-item/statics-item.component';

const HomePage = (): JSX.Element => {
  const theme = useTheme();
  const { data: getAdminCountQuery, loading: isGetAdminCountQueryLoading } =
    useGetAdminCountQuery();
  const { data: getUserCountQuery, loading: isGetUserCountQueryLoading } =
    useGetUserCountQuery();

  const createSubheader = (): JSX.Element => {
    return (
      <Typography variant="glassTitle" align="center" color="text.secondary">
        Website statistics
      </Typography>
    );
  };

  return (
    <Container maxWidth="sm" sx={{ zIndex: 10 }}>
      <List
        sx={{
          width: '100%',
          bgcolor: 'glass.main',
          boxShadow: `0 4px 1vw ${theme.palette.shadow.main}`,
          backdropFilter: 'blur(16px)',
          borderRadius: '1rem',
        }}
        subheader={createSubheader()}
      >
        <StaticsItemComponent
          loading={isGetAdminCountQueryLoading}
          icon={<GroupRounded color="primary" />}
          primary={'Registered Admin Accounts'}
          secondary={getAdminCountQuery?.getAdminCount ?? '-'}
        />
        <Divider variant="inset" component="li" />
        <StaticsItemComponent
          loading={isGetUserCountQueryLoading}
          icon={<AccountCircle color="primary" />}
          primary={'Registered User Accounts'}
          secondary={getUserCountQuery?.getUserCount ?? '-'}
        />
      </List>
    </Container>
  );
};

export default HomePage;

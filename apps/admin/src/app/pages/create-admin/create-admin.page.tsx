import { toast } from 'react-hot-toast';
import {
  FindAllUserDocument,
  useCreateAdminMutation,
} from '@user-handling/data-access';
import { translateErrorCodeToMessage } from '@user-handling/common';
import { CreateAdminFormValues } from '@/components/create-admin/create-admin-form.schema';
/* MUI */
import { Grid, Typography, useTheme } from '@mui/material';
/* COMPONENTS */
import RandomBackgroundComponent from '@/components/random-background/random-background.component';
import CreateAdminFormComponent from '@/components/create-admin/create-admin-form.component';

const CreateUserPage = (): JSX.Element => {
  const theme = useTheme();

  const [createAdminMutation, { loading }] = useCreateAdminMutation();

  const onCreate = async (data: CreateAdminFormValues): Promise<void> => {
    await createAdminMutation({
      variables: { createUserInput: { ...data } },
      refetchQueries: [
        {
          query: FindAllUserDocument,
          fetchPolicy: 'network-only',
        },
      ],
    })
      .then(() => {
        toast.success('A new admin account has been successfully created.');
      })
      .catch((error) => {
        console.log(error.message);
        toast.error(translateErrorCodeToMessage(error.message));
      });
  };

  return (
    <Grid
      container
      sx={{
        bgcolor: 'background.paper',
      }}
      className="relative w-full h-[100dvh] max-h-[100dvh] flex items-center justify-center overflow-clip"
    >
      <RandomBackgroundComponent
        circleCount={50}
        sizeRange={{
          min: 1,
          max: 10,
        }}
        zIndexRange={{
          min: 1,
          max: 9,
        }}
      />
      <Grid
        container
        item
        justifyContent="center"
        alignItems="center"
        direction="column"
        gap={8}
        zIndex={10}
        className="px-4 md:px-8 py-8"
        sx={{
          bgcolor: 'glass.main',
          boxShadow: `0 4px 1vw ${theme.palette.shadow.main}`,
          backdropFilter: 'blur(16px)',
          borderRadius: '1rem',
        }}
        xs={11}
        sm={7}
        md={5}
        lg={4}
        xl={3}
      >
        <Grid item sx={{ color: 'text.secondary' }}>
          <Typography variant="glassTitle">Create Admin</Typography>
        </Grid>
        <Grid item sx={{ color: 'text.primary' }}>
          <CreateAdminFormComponent
            onCreate={onCreate}
            parentProcessing={loading}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CreateUserPage;

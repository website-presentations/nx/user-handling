import { useLocation } from 'react-router-dom';

const usePathname = (): string => {
  const location = useLocation();
  return location.pathname;
};

export default usePathname;

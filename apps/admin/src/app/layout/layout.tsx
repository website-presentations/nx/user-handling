import React from 'react';
import { AuthManager } from '@/auth-manager/auth-manager';
import { UserRole, useWhoAmIQuery } from '@user-handling/data-access';
/* MUI */
import { Grid } from '@mui/material';
/* PAGES */
import LoginPage from '@/pages/login/login.page';
/* COMPONENTS */
import ProgressBarContainer from '@/components/progress-bar/progress-bar.container';
import RandomBackgroundComponent from '@/components/random-background/random-background.component';
import SidebarComponent from '@/components/menu/sidebar/sidebar.component';
import MobileMenuComponent from '@/components/menu/mobile-menu/mobile-menu.component';

type Prop = {
  children: React.ReactNode;
};

const Layout = (prop: Prop): JSX.Element => {
  const { children } = prop;
  const { data: whoAmIQuery, loading: isWhoAmIQueryLoading } = useWhoAmIQuery({
    fetchPolicy: 'cache-first',
  });

  if (isWhoAmIQueryLoading) {
    return <ProgressBarContainer />;
  }

  if (!whoAmIQuery?.whoAmI) {
    return <LoginPage />;
  }

  if (whoAmIQuery.whoAmI.role !== UserRole.Admin) {
    AuthManager.sharedInstance.logout();
    return <LoginPage />;
  }

  return (
    <Grid
      container
      item
      overflow="clip"
      className="bg-[#140F2D] flex flex-col-reverse md:flex-row flex-nowrap"
      position="relative"
    >
      <RandomBackgroundComponent
        circleCount={50}
        sizeRange={{
          min: 1,
          max: 10,
        }}
        zIndexRange={{
          min: 1,
          max: 9,
        }}
      />
      <SidebarComponent />
      <MobileMenuComponent />
      <Grid
        item
        bgcolor="background.paper"
        className={`relative flex flex-col justify-center items-center min-h-[100dvh] w-full overflow-clip bg-[#111111]`}
      >
        {children}
      </Grid>
    </Grid>
  );
};

export default Layout;

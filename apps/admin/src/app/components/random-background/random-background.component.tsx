import { NumericRange, getRandomInt } from '@user-handling/common';

type Props = {
  circleCount: number;
  sizeRange: NumericRange;
  zIndexRange: NumericRange;
};

const RandomBackgroundComponent = (props: Props): JSX.Element => {
  const { circleCount, sizeRange, zIndexRange } = props;
  let lastLeft = 0;
  let lastTop = 0;

  const generateRandomCoordinate = (last: number): number => {
    let random = getRandomInt({ min: 1, max: 100 });
    let min = Math.min(random, last);
    let max = Math.max(random, last);
    while (max - min < 30) {
      random = getRandomInt({ min: 1, max: 100 });
      min = Math.min(random, last);
      max = Math.max(random, last);
    }
    return random;
  };

  const generateRandomBackground = (): JSX.Element[] => {
    return Array.from(Array(circleCount).keys()).map((_, index) => {
      const size = getRandomInt(sizeRange);
      const zIndex = getRandomInt(zIndexRange);
      const opacity = getRandomInt({ min: 1, max: 7 });
      const left = generateRandomCoordinate(lastLeft);
      const top = generateRandomCoordinate(lastTop);
      lastLeft = left;
      lastTop = top;

      return (
        <div
          key={`bg-circle-${index}`}
          className={`absolute bg-[#8ab4f8] rounded-full`}
          style={{
            width: `${size}dvw`,
            height: `${size}dvh`,
            zIndex,
            opacity: `0.${opacity}`,
            left: `${left}dvw`,
            top: `${top}dvh`,
          }}
        />
      );
    });
  };

  return <>{generateRandomBackground()}</>;
};
export default RandomBackgroundComponent;

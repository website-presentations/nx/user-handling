import * as yup from 'yup';

export const CommonLoginFormSchema = yup.object({
  email: yup
    .string()
    .email('The email address format is incorrect.')
    .required('Entering an e-mail address is mandatory!'),
  password: yup.string().required('Entering a password is mandatory!'),
});

export type CommonLoginFormValues = {
  email: string;
  password: string;
};

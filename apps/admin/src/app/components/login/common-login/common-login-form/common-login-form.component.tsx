import { useEffect } from 'react';
/* FORM HANDLING */
import toast from 'react-hot-toast';
import { yupResolver } from '@hookform/resolvers/yup';
import { SubmitHandler, useForm } from 'react-hook-form';
import {
  CommonLoginFormSchema,
  CommonLoginFormValues,
} from './common-login-form.schema';
/* MUI */
import LoadingButton from '@mui/lab/LoadingButton';
import { TextField, Grid } from '@mui/material';
import { Login } from '@mui/icons-material';

type Props = {
  login: (details: CommonLoginFormValues) => Promise<void>;
  parentProcessing: boolean;
};

const LoginFormComponent = (props: Props): JSX.Element => {
  const { login, parentProcessing } = props;

  const checkIsDisabled = (): boolean => {
    return parentProcessing;
  };

  const {
    trigger,
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CommonLoginFormValues>({
    resolver: yupResolver(CommonLoginFormSchema),
    reValidateMode: 'onSubmit',
  });

  useEffect(() => {
    register('email');
    register('password');
  });

  const onSubmit: SubmitHandler<CommonLoginFormValues> = async (data) => {
    const { email, password } = data;

    if (!email || !password) {
      toast.error('Fill in the required data!');
      return;
    }

    await login({ email, password });
  };

  return (
    <form
      className="w-full"
      onSubmit={async (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (parentProcessing || !(await trigger())) return;

        await handleSubmit(onSubmit)(event);
      }}
    >
      <Grid container justifyContent="center" spacing={4}>
        <Grid item xs={11}>
          <TextField
            error={!!errors.email}
            fullWidth
            color="primary"
            id="email"
            type="email"
            label="E-mail"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue=""
            helperText={errors.email?.message ?? ''}
            onChange={(e) => {
              setValue('email', e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={11}>
          <TextField
            error={!!errors.password}
            fullWidth
            id="password"
            type="password"
            label="Password"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue=""
            helperText={errors.password?.message ?? ''}
            onChange={(e) => {
              setValue('password', e.target.value);
            }}
          />
        </Grid>
        <Grid display="flex" justifyContent="center" item xs={11}>
          <LoadingButton
            color="primary"
            type="submit"
            loading={parentProcessing}
            loadingPosition="end"
            endIcon={<Login />}
            variant="outlined"
            disabled={checkIsDisabled()}
            size="large"
            sx={{ fontWeight: 'bold' }}
          >
            <span className="pt-1.5">Login</span>
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default LoginFormComponent;

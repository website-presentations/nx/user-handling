import { useNavigate } from 'react-router-dom';
import toast from 'react-hot-toast';
/* LIBS */
import { translateErrorCodeToMessage } from '@user-handling/common';
import {
  WhoAmIDocument,
  useAdminLoginMutation,
} from '@user-handling/data-access';
/* MUI */
import { Grid, Typography, useTheme } from '@mui/material';
/* FORM */
import { CommonLoginFormValues } from './common-login-form/common-login-form.schema';
import LoginFormComponent from './common-login-form/common-login-form.component';
/* Background */
import RandomBackgroundComponent from '@/components/random-background/random-background.component';

const CommonLoginComponent = (): JSX.Element => {
  const [adminLoginMutation, { loading }] = useAdminLoginMutation();
  const theme = useTheme();

  const navigate = useNavigate();
  const login = async (data: CommonLoginFormValues): Promise<void> => {
    await adminLoginMutation({
      variables: { email: data.email, password: data.password },
      refetchQueries: [
        {
          query: WhoAmIDocument,
          fetchPolicy: 'network-only',
        },
      ],
    })
      .then(() => {
        toast.success('You have successfully logged in, welcome.');
        navigate('/');
      })
      .catch((error) => {
        console.log(error.message);
        toast.error(translateErrorCodeToMessage(error.message));
      });
  };

  return (
    <Grid
      container
      sx={{
        bgcolor: 'background.paper',
      }}
      className="relative w-full h-[100dvh] max-h-[100dvh] flex items-center justify-center overflow-clip"
    >
      <RandomBackgroundComponent
        circleCount={50}
        sizeRange={{
          min: 1,
          max: 10,
        }}
        zIndexRange={{
          min: 1,
          max: 9,
        }}
      />
      <Grid
        container
        item
        justifyContent="center"
        alignItems="center"
        direction="column"
        gap={8}
        zIndex={10}
        className="px-4 md:px-8 py-8"
        sx={{
          bgcolor: 'background.glass',
          boxShadow: `0 4px 30px ${theme.palette.background.default}`,
          backdropFilter: 'blur(16px)',
          borderRadius: '1rem',
        }}
        xs={11}
        sm={7}
        md={5}
        lg={4}
        xl={3}
      >
        <Grid item sx={{ color: 'text.secondary' }}>
          <Typography variant="glassTitle">Login</Typography>
        </Grid>
        <Grid item sx={{ color: 'text.primary' }}>
          <LoginFormComponent login={login} parentProcessing={loading} />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CommonLoginComponent;

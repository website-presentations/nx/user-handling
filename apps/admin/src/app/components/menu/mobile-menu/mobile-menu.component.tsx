import React from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthManager } from '@/auth-manager/auth-manager';
import { userMenuItems } from '../menu-items';
import { MenuItem } from '../menu-item.type';
/* MUI */
import { Box, Backdrop, SpeedDial, SpeedDialAction } from '@mui/material';
import { HomeRounded, LogoutRounded, MenuRounded } from '@mui/icons-material';

type State = {
  isOpen: boolean;
};

const initial: State = {
  isOpen: false,
};

const MobileMenuComponent = (): JSX.Element => {
  const [state, setState] = React.useState<State>(initial);
  const navigate = useNavigate();

  const handleOpen = () => setState((prev) => ({ ...prev, isOpen: true }));

  const handleClose = () => setState((prev) => ({ ...prev, isOpen: false }));

  const handleNavigate = (path: string) => {
    handleClose();
    navigate(path);
  };

  const handleLogout = () => {
    handleClose();
    AuthManager.sharedInstance.logout();
    navigate('/login');
  };

  return (
    <>
      <Backdrop
        open={state.isOpen}
        sx={{ bgcolor: 'glass.main', zIndex: 1499 }}
      />
      <Box
        sx={{
          height: 330,
          transform: 'translateZ(0px)',
          flexGrow: 1,
          position: 'absolute',
          bottom: '15px',
          right: '0',
          zIndex: 1500,
        }}
        className="block md:hidden"
      >
        <SpeedDial
          ariaLabel="Mobile menu"
          sx={{ position: 'absolute', bottom: 16, right: 16 }}
          icon={<MenuRounded />}
          onClose={handleClose}
          onOpen={handleOpen}
          open={state.isOpen}
        >
          <SpeedDialAction
            key={'home-menu-item-mobile'}
            icon={<HomeRounded />}
            onClick={() => handleNavigate('/')}
          />
          {userMenuItems.map((item: MenuItem) => (
            <SpeedDialAction
              key={`${item.key}-mobile`}
              icon={item.icon}
              onClick={() => handleNavigate(item.path)}
            />
          ))}
          <SpeedDialAction
            key={'logout-menu-item-mobile'}
            icon={<LogoutRounded />}
            onClick={() => handleLogout()}
          />
        </SpeedDial>
      </Box>
    </>
  );
};

export default MobileMenuComponent;

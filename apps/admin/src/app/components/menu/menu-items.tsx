import { MenuItem } from './menu-item.type';
/* MUI */
import { GroupRounded, PersonAddRounded } from '@mui/icons-material';

export const userMenuItems: MenuItem[] = [
  {
    key: 'users-list-menu-item',
    title: 'Users',
    icon: <GroupRounded />,
    path: '/users',
  },
  {
    key: 'create-admin-menu-item',
    title: 'Create Admin',
    icon: <PersonAddRounded />,
    path: '/create-admin',
  },
];

export type MenuItem = {
  key: string;
  title: string;
  icon: JSX.Element;
  path: string;
};

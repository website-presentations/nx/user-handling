import { useNavigate } from 'react-router-dom';
import { MenuItem } from '../../menu-item.type';
/* MUI */
import {
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@mui/material';

type Props = {
  item: MenuItem;
  isSidebarOpen: boolean;
  isActive: boolean;
};

export function SideBarItemComponent(props: Props) {
  const { item, isSidebarOpen, isActive } = props;
  const navigate = useNavigate();

  const listItemStyle = () => {
    const general = {
      display: 'block',
    };
    if (!isActive) {
      return general;
    }
    return {
      ...general,
      backgroundColor: 'glass.active',
      color: 'text.active',
    };
  };

  const listItemButtonStyle = () => {
    const general = {
      minHeight: 48,
      justifyContent: isSidebarOpen ? 'initial' : 'center',
      px: 2.5,
    };
    if (!isActive) {
      return general;
    }
    return {
      ...general,
      backgroundColor: 'glass.active',
      color: 'text.active',
    };
  };

  const listItemIconStyle = () => {
    const general = {
      minWidth: 0,
      mr: isSidebarOpen ? 3 : 'auto',
      justifyContent: 'center',
    };
    if (!isActive) {
      return general;
    }
    return {
      ...general,
      backgroundColor: 'glass.active',
      color: 'text.active',
      fontWeight: 'bold',
    };
  };

  const listItemPrimaryText = () => {
    return (
      <Typography
        typography={isActive ? 'menuItemActive' : 'menuItem'}
        sx={{ opacity: isSidebarOpen ? 1 : 0 }}
      >
        {item.title}
      </Typography>
    );
  };

  return (
    <ListItem
      onClick={() => navigate(item.path)}
      disablePadding
      sx={listItemStyle()}
    >
      <ListItemButton sx={listItemButtonStyle()}>
        <ListItemIcon sx={listItemIconStyle()}>{item.icon}</ListItemIcon>
        <ListItemText primary={listItemPrimaryText()} />
      </ListItemButton>
    </ListItem>
  );
}

export default SideBarItemComponent;

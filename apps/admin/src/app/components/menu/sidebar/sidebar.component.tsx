import React from 'react';
import { AuthManager } from '@/auth-manager/auth-manager';
import { useNavigate } from 'react-router-dom';
import usePathname from '@/hooks/use-pathname/use-pathname';
import { MenuItem } from '../menu-item.type';
import { userMenuItems } from '../menu-items';
import MenuItemComponent from './sidebar-item/menu-item.component';
/* MUI */
import {
  ChevronLeft,
  ChevronRight,
  LogoutRounded,
  HomeRounded,
} from '@mui/icons-material';
import MuiDrawer from '@mui/material/Drawer';
import {
  IconButton,
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  styled,
  useTheme,
  Theme,
  CSSObject,
} from '@mui/material';

type State = {
  isOpen: boolean;
};

const initial: State = {
  isOpen: false,
};

const SidebarComponent = (): JSX.Element => {
  const theme = useTheme();
  const navigate = useNavigate();
  const currentPathname = usePathname();

  const drawerWidth = 240;
  const [state, setState] = React.useState<State>(initial);

  const openedMixin = (theme: Theme): CSSObject => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
  });

  const closedMixin = (theme: Theme): CSSObject => ({
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
      width: `calc(${theme.spacing(8)} + 1px)`,
    },
  });

  const Drawer = styled(MuiDrawer, {
    shouldForwardProp: (prop) => prop !== 'isSidebarOpen',
  })(({ theme, open: isSidebarOpen }) => ({
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    ...(isSidebarOpen && {
      ...openedMixin(theme),
      position: 'absolute',
      '& .MuiDrawer-paper': openedMixin(theme),
    }),
    ...(!isSidebarOpen && {
      ...closedMixin(theme),
      position: 'relative',
      '& .MuiDrawer-paper': closedMixin(theme),
    }),
  }));

  const handleDrawerOpen = () => {
    setState((prev) => ({ ...prev, isOpen: true }));
  };

  const handleDrawerClose = () => {
    setState((prev) => ({ ...prev, isOpen: false }));
  };

  const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  }));

  const getIsActive = (pathname: string): boolean => {
    return currentPathname === pathname;
  };

  return (
    <Drawer variant="permanent" open={state.isOpen} className="hidden md:block">
      <DrawerHeader>
        {state.isOpen ? (
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRight /> : <ChevronLeft />}
          </IconButton>
        ) : (
          <IconButton onClick={handleDrawerOpen}>
            {theme.direction === 'rtl' ? <ChevronLeft /> : <ChevronRight />}
          </IconButton>
        )}
      </DrawerHeader>
      <Divider />
      <List>
        <MenuItemComponent
          key={'home'}
          item={{
            key: 'home',
            path: '/',
            title: 'Home',
            icon: <HomeRounded />,
          }}
          isSidebarOpen={state.isOpen}
          isActive={getIsActive('/')}
        />
      </List>
      <Divider />
      <List>
        {userMenuItems.map((item: MenuItem) => (
          <MenuItemComponent
            key={item.key}
            item={item}
            isSidebarOpen={state.isOpen}
            isActive={getIsActive(item.path)}
          />
        ))}
      </List>
      <Divider />
      <List>
        <ListItem key="logout" disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: state.isOpen ? 'initial' : 'center',
              px: 2.5,
            }}
            onClick={() => {
              AuthManager.sharedInstance.logout();
              navigate('/login');
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: state.isOpen ? 3 : 'auto',
                justifyContent: 'center',
              }}
            >
              <LogoutRounded />
            </ListItemIcon>
            <ListItemText
              primary="Logout"
              sx={{ opacity: state.isOpen ? 1 : 0 }}
            />
          </ListItemButton>
        </ListItem>
      </List>
    </Drawer>
  );
};

export default SidebarComponent;

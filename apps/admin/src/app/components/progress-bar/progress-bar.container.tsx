import ProgressBarComponent from './progress-bar.component';

const ProgressBarContainer = (): JSX.Element => {
  return (
    <div className="w-full h-[100dvh] fixed flex flex-col gap-5 z-50 justify-center items-center bg-[#000000bd]">
      <ProgressBarComponent />
      <p className="text-3xl font-bold text-[#3F88C5] tracking-widest">
        Loading
      </p>
    </div>
  );
};

export default ProgressBarContainer;

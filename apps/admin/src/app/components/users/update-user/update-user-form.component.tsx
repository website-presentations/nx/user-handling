import { useEffect } from 'react';
import toast from 'react-hot-toast';
import { removeVoidProps } from '@user-handling/common';
import { UserDto } from '@user-handling/data-access';
/* FORM VALIDATION */
import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import {
  UpdateUserFormSchema,
  UpdateUserFormValues,
} from './update-user-form.schema';
/* MUI */
import LoadingButton from '@mui/lab/LoadingButton';
import { TextField, Grid } from '@mui/material';
import { EditOutlined } from '@mui/icons-material';

type Props = {
  onUpdate: (details: UpdateUserFormValues) => Promise<void>;
  user: UserDto;
  parentProcessing: boolean;
};

const UpdateUserFormComponent = (props: Props): JSX.Element => {
  const { onUpdate, user, parentProcessing } = props;

  const checkIsDisabled = (): boolean => {
    return parentProcessing;
  };
  const defaultValues: UpdateUserFormValues =
    removeVoidProps<UpdateUserFormValues>({
      firstName: user.firstName ?? void 0,
      lastName: user.lastName ?? void 0,
      email: user.email,
    });

  const {
    trigger,
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdateUserFormValues>({
    defaultValues,
    resolver: yupResolver(UpdateUserFormSchema),
    reValidateMode: 'onSubmit',
  });

  useEffect(() => {
    register('firstName');
    register('lastName');
    register('email');
  });

  const onSubmit: SubmitHandler<UpdateUserFormValues> = async (data) => {
    const { firstName, lastName, email } = data;

    if (!email) {
      toast.error('Fill in the required data!');
      return;
    }

    await onUpdate(
      removeVoidProps({
        email,
        firstName: firstName ?? void 0,
        lastName: lastName ?? void 0,
      })
    );
  };

  return (
    <form
      className="w-full py-4"
      onSubmit={async (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (parentProcessing || !(await trigger())) return;

        await handleSubmit(onSubmit)(event);
      }}
    >
      <Grid container justifyContent="center" spacing={4}>
        <Grid item xs={11}>
          <TextField
            fullWidth
            id="firstName"
            type="text"
            label="Forename"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue={user.firstName}
            onChange={(e) => {
              setValue('firstName', e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={11}>
          <TextField
            fullWidth
            id="lastName"
            type="text"
            label="Surname"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue={user.lastName}
            onChange={(e) => {
              setValue('lastName', e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={11}>
          <TextField
            error={!!errors.email}
            fullWidth
            color="primary"
            id="email"
            type="email"
            label="E-mail"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue={user.email}
            helperText={errors.email?.message ?? ''}
            onChange={(e) => {
              setValue('email', e.target.value);
            }}
          />
        </Grid>
        <Grid display="flex" justifyContent="center" item xs={11}>
          <LoadingButton
            color="primary"
            type="submit"
            loading={parentProcessing}
            loadingPosition="end"
            endIcon={<EditOutlined />}
            variant="outlined"
            disabled={checkIsDisabled()}
            size="large"
            sx={{ fontWeight: 'bold' }}
          >
            <span className="pt-1.5">Update</span>
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default UpdateUserFormComponent;

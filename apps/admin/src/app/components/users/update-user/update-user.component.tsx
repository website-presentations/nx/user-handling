import { toast } from 'react-hot-toast';
import { translateErrorCodeToMessage } from '@user-handling/common';
import {
  FindAllUserDocument,
  UserDto,
  useUpdateUserMutation,
} from '@user-handling/data-access';
import UpdateUserFormComponent from './update-user-form.component';
import { UpdateUserFormValues } from './update-user-form.schema';
/* MUI */
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  useTheme,
} from '@mui/material';

type Props = {
  isOpen: boolean;
  handleClose: () => void;
  user: UserDto;
};

const UpdateUserComponent = (props: Props): JSX.Element => {
  const { isOpen, user, handleClose } = props;
  const theme = useTheme();

  const [updateUserMutation] = useUpdateUserMutation();

  const onUpdate = async (data: UpdateUserFormValues) => {
    await updateUserMutation({
      variables: {
        updateUserInput: {
          id: user.id,
          ...data,
        },
      },
      refetchQueries: [
        {
          query: FindAllUserDocument,
          variables: { userCriteriaInput: {} },
          fetchPolicy: 'network-only',
        },
      ],
    })
      .then(() => {
        toast.success('User account has been successfully updated.');
        handleClose();
      })
      .catch((error) => {
        console.log(error.message);
        toast.error(translateErrorCodeToMessage(error.message));
      });
  };
  return (
    <Dialog
      sx={{
        bgcolor: 'glass.main',
        boxShadow: `0 4px 30px ${theme.palette.background.default}`,
        backdropFilter: 'blur(16px)',
        borderRadius: '1rem',
        zIndex: 2000,
      }}
      open={isOpen}
      onClose={handleClose}
    >
      <DialogTitle align="center">
        <Typography sx={{ color: 'text.secondary' }} variant="glassTitle">
          Update User
        </Typography>
      </DialogTitle>
      <DialogContent>
        <UpdateUserFormComponent
          onUpdate={onUpdate}
          user={user}
          parentProcessing={false}
        />
      </DialogContent>
    </Dialog>
  );
};

export default UpdateUserComponent;

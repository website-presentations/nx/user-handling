import { UserDto } from '@user-handling/data-access';
/* MUI */
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography,
  useTheme,
} from '@mui/material';
/* COMPONENTS */
import UsersTableRowComponent from './users-table-row.component';

type Props = {
  users: UserDto[];
};

const UsersTableComponent = (props: Props): JSX.Element => {
  const { users } = props;
  const theme = useTheme();

  return (
    <TableContainer
      sx={{
        bgcolor: 'glass.main',
        boxShadow: `0 4px 1vw ${theme.palette.shadow.main}`,
        backdropFilter: 'blur(16px)',
        borderRadius: '1rem',
      }}
      component={Paper}
    >
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell align="center" sx={{ color: 'text.secondary' }}>
              <Typography variant="glassTitle">Users</Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((user: UserDto) => (
            <UsersTableRowComponent key={`${user.email}-row`} user={user} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default UsersTableComponent;

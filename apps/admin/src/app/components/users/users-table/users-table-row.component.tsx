import React from 'react';
import { UserDto } from '@user-handling/data-access';
/* MUI */
import {
  ManageAccountsRounded,
  PersonRemoveAlt1Rounded,
} from '@mui/icons-material';
import {
  TableRow,
  TableCell,
  Collapse,
  Box,
  Typography,
  Table,
  TableBody,
  Button,
  IconButton,
} from '@mui/material';
import UpdateUserComponent from '../update-user/update-user.component';
import RemoveUserComponent from '../remove-user/remove-user.component';

type Props = {
  user: UserDto;
};

type State = {
  isOpen: boolean;
  isUpdate: boolean;
  isRemove: boolean;
};

const initial: State = {
  isOpen: false,
  isUpdate: false,
  isRemove: false,
};

const UsersTableRowComponent = (props: Props): JSX.Element => {
  const { user } = props;
  const [state, setState] = React.useState<State>(initial);

  const toggleDetails = () => {
    setState((prev) => ({ ...prev, isOpen: !prev.isOpen }));
  };

  const onUpdateOpen = () => {
    setState((prev) => ({ ...prev, isUpdate: true }));
  };

  const onUpdateClose = () => {
    setState((prev) => ({ ...prev, isUpdate: false }));
  };

  const onRemoveOpen = () => {
    setState((prev) => ({ ...prev, isRemove: true }));
  };

  const onRemoveClose = () => {
    setState((prev) => ({ ...prev, isRemove: false }));
  };

  return (
    <React.Fragment>
      <UpdateUserComponent
        isOpen={state.isUpdate}
        handleClose={onUpdateClose}
        user={user}
      />
      <RemoveUserComponent
        isOpen={state.isRemove}
        handleClose={onRemoveClose}
        user={user}
      />
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <Button
            aria-label="expand row"
            size="small"
            sx={{ color: 'text.primary' }}
            onClick={() => toggleDetails()}
          >
            <Typography variant="userListButton" className="text-xs">
              {user.email}
            </Typography>
          </Button>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell align="center" style={{ paddingBottom: 0, paddingTop: 0 }}>
          <Collapse in={state.isOpen} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography
                variant="glassTitle"
                sx={{ color: 'text.secondary' }}
                gutterBottom
                component="div"
              >
                User details
              </Typography>
              <Table size="medium" aria-label="details">
                <TableBody>
                  <TableRow key={`${user.id}-firstName`}>
                    <TableCell component="th" scope="row">
                      <Typography
                        variant="userListRowTitle"
                        sx={{ color: 'text.secondary' }}
                      >
                        Forename:
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="userListRowValue"
                        sx={{ color: 'text.secondary' }}
                      >
                        {user.firstName ?? '-'}
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow key={`${user.id}-lastName`}>
                    <TableCell component="th" scope="row">
                      <Typography
                        variant="userListRowTitle"
                        sx={{ color: 'text.secondary' }}
                      >
                        Surname:
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="userListRowValue"
                        sx={{ color: 'text.secondary' }}
                      >
                        {user.lastName ?? '-'}
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow key={`${user.id}-role`}>
                    <TableCell component="th" scope="row">
                      <Typography
                        variant="userListRowTitle"
                        sx={{ color: 'text.secondary' }}
                      >
                        Role:
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="userListRowValue"
                        sx={{ color: 'text.secondary' }}
                      >
                        {user.role}
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow key={`${user.id}-status`}>
                    <TableCell component="th" scope="row">
                      <Typography
                        variant="userListRowTitle"
                        sx={{ color: 'text.secondary' }}
                      >
                        Status:
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="userListRowValue"
                        sx={{ color: 'text.secondary' }}
                      >
                        {user.status}
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow key={`${user.id}-actions`}>
                    <TableCell component="th" scope="row" align="center">
                      <IconButton onClick={onUpdateOpen}>
                        <ManageAccountsRounded />
                      </IconButton>
                    </TableCell>
                    <TableCell component="th" scope="row" align="center">
                      <IconButton onClick={onRemoveOpen}>
                        <PersonRemoveAlt1Rounded />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

export default UsersTableRowComponent;

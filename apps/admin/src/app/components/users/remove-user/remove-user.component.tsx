import { toast } from 'react-hot-toast';
import { translateErrorCodeToMessage } from '@user-handling/common';
import {
  FindAllUserDocument,
  UserDto,
  useRemoveUserMutation,
} from '@user-handling/data-access';
/* MUI */
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  useTheme,
  DialogContentText,
  Button,
  DialogActions,
} from '@mui/material';

type Props = {
  isOpen: boolean;
  handleClose: () => void;
  user: UserDto;
};

const RemoveUserComponent = (props: Props): JSX.Element => {
  const { isOpen, user, handleClose } = props;
  const theme = useTheme();

  const [removeUserMutation] = useRemoveUserMutation();

  const onRemove = async () => {
    await removeUserMutation({
      variables: {
        id: user.id,
      },
      refetchQueries: [
        {
          query: FindAllUserDocument,
          variables: { userCriteriaInput: {} },
          fetchPolicy: 'network-only',
        },
      ],
    })
      .then(() => {
        toast.success('User account has been successfully removed.');
        handleClose();
      })
      .catch((error) => {
        console.log(error.message);
        toast.error(translateErrorCodeToMessage(error.message));
      });
  };
  return (
    <Dialog
      sx={{
        bgcolor: 'glass.main',
        boxShadow: `0 4px 30px ${theme.palette.background.default}`,
        backdropFilter: 'blur(16px)',
        borderRadius: '1rem',
        zIndex: 2000,
      }}
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="remove-user-dialog-title"
      aria-describedby="remove-user-dialog-description"
    >
      <DialogTitle align="center" id="remove-user-dialog-title">
        <Typography sx={{ color: 'text.secondary' }} variant="glassTitle">
          Remove User
        </Typography>
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="remove-user-dialog-description" align="center">
          <Typography>
            Are you sure you want to delete the following user?
          </Typography>
          <Typography
            sx={{
              color: 'text.primary',
              fontWeight: 'bold',
              wordBreak: 'break-word',
              marginTop: '2rem',
            }}
          >
            {user.email}
          </Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={onRemove} autoFocus>
          Remove
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default RemoveUserComponent;

/* MUI */
import {
  ListItem,
  ListItemAvatar,
  Avatar,
  Skeleton,
  ListItemText,
  Typography,
} from '@mui/material';

type Props = {
  loading: boolean;
  icon: JSX.Element;
  primary: string;
  secondary: string | number | boolean;
};

export const StaticsItemComponent = (props: Props): JSX.Element => {
  const { loading, icon, primary, secondary } = props;

  const renderIcon = (): JSX.Element => {
    return loading ? <Skeleton variant="circular" /> : icon;
  };

  const renderPrimary = (): JSX.Element => {
    return loading ? (
      <Skeleton variant="text" width="75%" />
    ) : (
      <Typography variant="tableSecondary" color="text.primary">
        {primary}
      </Typography>
    );
  };

  const renderSecondary = (): JSX.Element => {
    return loading ? (
      <Skeleton variant="text" width="30%" />
    ) : (
      <Typography variant="tableSecondary" color="text.secondary">
        {secondary}
      </Typography>
    );
  };

  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar>{renderIcon()}</Avatar>
      </ListItemAvatar>
      <ListItemText primary={renderPrimary()} secondary={renderSecondary()} />
    </ListItem>
  );
};

export default StaticsItemComponent;

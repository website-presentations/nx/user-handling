import { Maybe } from '@user-handling/common';
import * as yup from 'yup';

export const CreateAdminFormSchema = yup.object({
  email: yup
    .string()
    .email('The email address format is incorrect.')
    .required('Entering an e-mail address is mandatory!'),
});

export type CreateAdminFormValues = {
  email: string;
  firstName?: Maybe<string>;
  lastName?: Maybe<string>;
  password?: Maybe<string>;
};

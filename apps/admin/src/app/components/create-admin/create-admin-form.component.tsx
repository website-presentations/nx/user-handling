import { useEffect } from 'react';
import toast from 'react-hot-toast';
import { removeVoidProps } from '@user-handling/common';
/* FORM VALIDATION */
import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import {
  CreateAdminFormSchema,
  CreateAdminFormValues,
} from './create-admin-form.schema';
/* MUI */
import LoadingButton from '@mui/lab/LoadingButton';
import { TextField, Grid } from '@mui/material';
import { AddOutlined } from '@mui/icons-material';

type Props = {
  onCreate: (details: CreateAdminFormValues) => Promise<void>;
  parentProcessing: boolean;
};

const CreateAdminFormComponent = (props: Props): JSX.Element => {
  const { onCreate, parentProcessing } = props;

  const checkIsDisabled = (): boolean => {
    return parentProcessing;
  };

  const {
    trigger,
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CreateAdminFormValues>({
    resolver: yupResolver(CreateAdminFormSchema),
    reValidateMode: 'onSubmit',
  });

  useEffect(() => {
    register('firstName');
    register('lastName');
    register('email');
    register('password');
  });

  const onSubmit: SubmitHandler<CreateAdminFormValues> = async (data) => {
    const { firstName, lastName, email, password } = data;

    if (!email) {
      toast.error('Fill in the required data!');
      return;
    }

    await onCreate(
      removeVoidProps({
        email,
        password: password ?? void 0,
        firstName: firstName ?? void 0,
        lastName: lastName ?? void 0,
      })
    );
  };

  return (
    <form
      className="w-full"
      onSubmit={async (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (parentProcessing || !(await trigger())) return;

        await handleSubmit(onSubmit)(event);
      }}
    >
      <Grid container justifyContent="center" spacing={4}>
        <Grid item xs={11}>
          <TextField
            fullWidth
            id="firstName"
            type="text"
            label="Forename"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue=""
            onChange={(e) => {
              setValue('firstName', e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={11}>
          <TextField
            fullWidth
            id="lastName"
            type="text"
            label="Surname"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue=""
            onChange={(e) => {
              setValue('lastName', e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={11}>
          <TextField
            error={!!errors.email}
            fullWidth
            color="primary"
            id="email"
            type="email"
            label="E-mail"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue=""
            helperText={errors.email?.message ?? ''}
            onChange={(e) => {
              setValue('email', e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={11}>
          <TextField
            fullWidth
            id="password"
            type="password"
            label="Password"
            InputLabelProps={{
              sx: { color: 'text.primary', fontWeight: 'bold' },
            }}
            defaultValue=""
            onChange={(e) => {
              setValue('password', e.target.value);
            }}
          />
        </Grid>
        <Grid display="flex" justifyContent="center" item xs={11}>
          <LoadingButton
            color="primary"
            type="submit"
            loading={parentProcessing}
            loadingPosition="end"
            endIcon={<AddOutlined />}
            variant="outlined"
            disabled={checkIsDisabled()}
            size="large"
            sx={{ fontWeight: 'bold' }}
          >
            <span className="pt-1.5">Create</span>
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default CreateAdminFormComponent;

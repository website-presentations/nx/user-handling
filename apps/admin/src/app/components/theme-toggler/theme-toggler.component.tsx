/* MUI */
import { IconButton } from '@mui/material';
import { LightbulbOutlined, DarkModeOutlined } from '@mui/icons-material';

type Props = {
  mode: string;
  onClick: () => void;
};

const ThemeTogglerComponent = (props: Props) => {
  const { mode, onClick } = props;

  return (
    <div className="absolute top-5 right-5 z-50">
      <IconButton sx={{ ml: 1 }} onClick={onClick} color="inherit">
        {mode === 'dark' ? (
          <LightbulbOutlined color="primary" fontSize="large" />
        ) : (
          <DarkModeOutlined color="primary" fontSize="large" />
        )}
      </IconButton>
    </div>
  );
};

export default ThemeTogglerComponent;

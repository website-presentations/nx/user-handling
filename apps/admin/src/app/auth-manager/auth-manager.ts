import toast from 'react-hot-toast';
import { ApolloClient } from '@apollo/client';
import {
  LogoutDocument,
  LogoutMutation,
  WhoAmIDocument,
} from '@user-handling/data-access';

export class AuthManager {
  static sharedInstance = new AuthManager();

  private apollo!: ApolloClient<unknown>;

  set apolloClient(client: ApolloClient<unknown>) {
    this.apollo = client;
  }

  async clearStorage(): Promise<void> {
    this.apollo.clearStore().catch((e) => {
      console.error('Something went wrong!');
      throw e;
    });
  }

  async logout(): Promise<void> {
    const res = await this.apollo
      .mutate<LogoutMutation>({
        mutation: LogoutDocument,
        refetchQueries: [
          {
            query: WhoAmIDocument,
            fetchPolicy: 'network-only',
          },
        ],
      })
      .then((res) => !!res.data?.logout);

    if (!res) {
      throw new Error('Failed to log out!');
    }
    toast.success('You have successfully signed out.');
    this.clearStorage();
  }
}

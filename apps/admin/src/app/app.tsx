import React from 'react';
import {
  ApolloClient,
  ApolloLink,
  ApolloProvider,
  HttpLink,
  InMemoryCache,
} from '@apollo/client';
import { AuthManager } from '@/auth-manager/auth-manager';
import { environment } from '../environments/environment';
import { Toaster } from 'react-hot-toast';
import { Route, Routes } from 'react-router-dom';
import Layout from './layout/layout';
/* MUI */
import { createTheme, Theme, ThemeProvider } from '@mui/material';
/* Theme */
import { getPaletteOptions } from '@/theme/palette-options';
import { getTypographyOptions } from '@/theme/typography-options';
import { getComponentOverrides } from '@/theme/component-overrides';
/* Pages */
import HomePage from '@/pages/home/home.page';
import LoginPage from '@/pages/login/login.page';
import UsersPage from '@/pages/users/users.page';
import CreateAdminPage from '@/pages/create-admin/create-admin.page';
/* COMPONENTS */
import ThemeTogglerComponent from '@/components/theme-toggler/theme-toggler.component';

const ColorModeContext = React.createContext({
  toggleColorMode: () => {
    return;
  },
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: ApolloLink.from([
    new HttpLink({
      uri: environment.backendURL,
      credentials: 'include',
    }),
  ]),
});

AuthManager.sharedInstance.apolloClient = client;

export function App() {
  const [mode, setMode] = React.useState<'light' | 'dark'>('dark');
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    []
  );

  const theme: Theme = React.useMemo(
    () =>
      createTheme({
        ...getPaletteOptions(mode),
        ...getTypographyOptions(),
        ...getComponentOverrides(),
      }),
    [mode]
  );

  return (
    <ApolloProvider client={client}>
      <ColorModeContext.Provider value={colorMode}>
        <ThemeProvider theme={theme}>
          <Toaster position="top-center" reverseOrder={false} />
          <ThemeTogglerComponent
            mode={theme.palette.mode}
            onClick={colorMode.toggleColorMode}
          />
          <Routes>
            <Route
              path="/"
              element={
                <Layout>
                  <HomePage />
                </Layout>
              }
            />
            <Route
              path="/users"
              element={
                <Layout>
                  <UsersPage />
                </Layout>
              }
            />
            <Route
              path="/create-admin"
              element={
                <Layout>
                  <CreateAdminPage />
                </Layout>
              }
            />
            <Route path="/login" element={<LoginPage />} />
          </Routes>
        </ThemeProvider>
      </ColorModeContext.Provider>
    </ApolloProvider>
  );
}

export default App;

import { Theme } from '@mui/material';

export const getComponentOverrides = () => ({
  components: {
    // Name of the component
    MuiDrawer: {
      styleOverrides: {
        // Name of the slot
        root: ({ theme }: { theme: Theme }) => ({
          backgroundColor: theme.palette.background.paper,
        }),
        paperAnchorDockedLeft: ({ theme }: { theme: Theme }) => ({
          backdropFilter: 'blur(16px)',
          borderTopRightRadius: '1rem',
          borderBottomRightRadius: '1rem',
          backgroundColor: theme.palette.glass.main,
          boxShadow: `0 4px 1vw ${theme.palette.shadow.main}`,
        }),
      },
    },
    MuiTableCell: {
      styleOverrides: {
        // Name of the slot
        root: ({ theme }: { theme: Theme }) => ({
          padding: '1rem 10px',
        }),
      },
    },
  },
});

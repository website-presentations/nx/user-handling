declare module '@mui/material/styles' {
  interface TypographyVariants {
    glassTitle: React.CSSProperties;
    tableSecondary: React.CSSProperties;
    menuItem: React.CSSProperties;
    menuItemActive: React.CSSProperties;
    userListButton: React.CSSProperties;
    userListRowTitle: React.CSSProperties;
    userListRowValue: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    glassTitle?: React.CSSProperties;
    tableSecondary?: React.CSSProperties;
    menuItem?: React.CSSProperties;
    menuItemActive?: React.CSSProperties;
    userListButton?: React.CSSProperties;
    userListRowTitle?: React.CSSProperties;
    userListRowValue?: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    glassTitle: true;
    tableSecondary: true;
    menuItem: true;
    menuItemActive: true;
    userListButton: true;
    userListRowTitle: true;
    userListRowValue: true;
  }
}

export const getTypographyOptions = () => ({
  typography: {
    glassTitle: {
      display: 'block',
      fontWeight: 700,
      paddingTop: '2rem',
      paddingBottom: '1rem',
      fontSize: '2.25rem',
      lineHeight: '2.5rem',
      letterSpacing: '0.05em',
    },
    tableSecondary: {
      display: 'block',
      fontSize: 'large',
      fontWeight: 700,
    },
    menuItem: {
      letterSpacing: '0.05em',
    },
    menuItemActive: {
      fontWeight: 700,
      letterSpacing: '0.05em',
    },
    userListButton: {
      fontWeight: 700,
      letterSpacing: '0.05em',
      padding: '0.5rem 1rem',
    },
    userListRowTitle: {
      fontWeight: 700,
      letterSpacing: '0.05em',
      padding: '1rem 0rem',
    },
    userListRowValue: {
      letterSpacing: '0.05em',
      padding: '1rem 0rem',
    },
  },
});

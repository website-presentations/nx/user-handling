import { PaletteMode } from '@mui/material';
import { yellow, lightBlue } from '@mui/material/colors';

declare module '@mui/material/styles' {
  interface Palette {
    glass: Palette['primary'];
    shadow: Palette['primary'];
  }

  interface PaletteOptions {
    glass: PaletteOptions['primary'];
    shadow: PaletteOptions['primary'];
  }

  interface PaletteColor {
    active?: string;
  }

  interface SimplePaletteColorOptions {
    active?: string;
  }
}

export const getPaletteOptions = (mode: PaletteMode) => ({
  palette: {
    mode,
    ...(mode === 'light'
      ? {
          // palette values for light mode
          primary: lightBlue,
          background: {
            default: '#0000DD36',
            paper: '#FFF',
          },
          glass: {
            main: '#99CCFF36',
            active: lightBlue[700],
          },
          shadow: {
            main: '#99CCFF36',
          },
          text: {
            primary: lightBlue[700],
            secondary: lightBlue[500],
            active: '#FFF',
          },
        }
      : {
          // palette values for dark mode
          primary: yellow,
          background: {
            default: '#fff59dC9',
            paper: '#003',
          },
          glass: {
            main: 'rgba(0, 0, 51, 0.75)',
            active: yellow[400],
          },
          shadow: {
            main: 'rgba(255, 245, 157, 0.79)',
          },
          action: {
            active: yellow[400],
            focus: yellow[400],
          },
          text: {
            primary: yellow[400],
            secondary: yellow[200],
            active: '#003',
          },
        }),
  },
});

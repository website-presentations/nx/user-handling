import { Maybe, UserRole, UserStatus } from '@user-handling/common';

export class UserDomain {
  id!: string;
  firstName?: Maybe<string>;
  lastName?: Maybe<string>;
  email!: string;
  password?: string;
  role!: UserRole;
  status!: UserStatus;
}

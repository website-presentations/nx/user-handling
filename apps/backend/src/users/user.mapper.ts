import { UserDto } from './dto/user.dto';
import { Injectable } from '@nestjs/common';
import { UserDocument } from './entities/user.entity';
import { UserDomain } from './domain/user.domain';

@Injectable()
export class UserMapper {
  persistenceToDomain(persistence: UserDocument): UserDomain {
    const domain: UserDomain = {
      id: persistence.id,
      firstName: persistence.firstName,
      lastName: persistence.lastName,
      email: persistence.email,
      password: persistence.password ?? void 0,
      role: persistence.role,
      status: persistence.status,
    };
    return Object.assign(new UserDomain(), domain);
  }

  domainToDto(domain: UserDomain): UserDto {
    const dto: UserDto = {
      id: domain.id,
      firstName: domain.firstName,
      lastName: domain.lastName,
      email: domain.email,
      role: domain.role,
      status: domain.status,
    };
    return Object.assign(new UserDto(), dto);
  }
}

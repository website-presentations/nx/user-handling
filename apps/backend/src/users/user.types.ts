import { UserDocument } from './entities/user.entity';

export type UserProp = keyof UserDocument;

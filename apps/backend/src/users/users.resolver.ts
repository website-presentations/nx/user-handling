import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { UserDto } from './dto/user.dto';
import { Maybe, UserError, UserRole } from '@user-handling/common';
import { UserMapper } from './user.mapper';
import { UserDomain } from './domain/user.domain';
import { UserCriteriaInput } from './dto/user-criteria.input';
import { BadRequestException, Logger, UseGuards } from '@nestjs/common';
import { JWTAdminGuard } from '../auth/guards/admin.guard';

@Resolver(() => UserDto)
export class UsersResolver {
  private readonly logger = new Logger(UsersResolver.name);

  constructor(
    private readonly usersService: UsersService,
    private readonly mapper: UserMapper
  ) {}

  @Query(() => [UserDto], { name: 'findAllUser' })
  @UseGuards(JWTAdminGuard)
  async findAllUser(
    @Args('userCriteriaInput') criteria: UserCriteriaInput
  ): Promise<UserDto[]> {
    const users: UserDomain[] = await this.usersService.findAll(criteria);
    return users.map((user: UserDomain) => this.mapper.domainToDto(user));
  }

  @Query(() => Number)
  @UseGuards(JWTAdminGuard)
  async getAdminCount(): Promise<number> {
    return this.usersService.countUsersBy({ role: UserRole.Admin });
  }

  @Query(() => Number)
  @UseGuards(JWTAdminGuard)
  async getUserCount(): Promise<number> {
    return this.usersService.countUsersBy({ role: UserRole.User });
  }

  @Query(() => UserDto, { name: 'findOneUser' })
  @UseGuards(JWTAdminGuard)
  async findOneUser(@Args('userCriteriaInput') criteria: UserCriteriaInput) {
    const user: Maybe<UserDomain> = await this.usersService.findOne(criteria);
    if (!user) {
      this.logger.warn('[findOneUser] Can not determine user with');
      this.logger.warn({ criteria });
      throw new BadRequestException(UserError.UserDoesNotExists);
    }
    return this.mapper.domainToDto(user);
  }

  @Mutation(() => Boolean)
  @UseGuards(JWTAdminGuard)
  async createAdmin(
    @Args('createUserInput') createUserInput: CreateUserInput
  ): Promise<boolean> {
    await this.usersService.create(createUserInput, UserRole.Admin);
    return true;
  }

  @Mutation(() => Boolean)
  @UseGuards(JWTAdminGuard)
  async createUser(
    @Args('createUserInput') createUserInput: CreateUserInput
  ): Promise<boolean> {
    await this.usersService.create(createUserInput, UserRole.User);
    return true;
  }

  @Mutation(() => Boolean)
  @UseGuards(JWTAdminGuard)
  async updateUser(
    @Args('updateUserInput') updateUserInput: UpdateUserInput
  ): Promise<boolean> {
    return this.usersService.update(updateUserInput);
  }

  @Mutation(() => Boolean)
  @UseGuards(JWTAdminGuard)
  async removeUser(
    @Args('id', { type: () => String }) id: string
  ): Promise<boolean> {
    return this.usersService.remove(id);
  }
}

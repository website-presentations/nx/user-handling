import { InputType, PartialType } from '@nestjs/graphql';
import { UpdateUserInput } from './update-user.input';

@InputType()
export class UserCriteriaInput extends PartialType(UpdateUserInput) {}

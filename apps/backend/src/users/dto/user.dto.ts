import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Maybe, UserRole, UserStatus } from '@user-handling/common';

@ObjectType({ description: 'User DTO' })
export class UserDto {
  @Field(() => ID)
  id!: string;

  @Field(() => String, { nullable: true })
  firstName?: Maybe<string>;

  @Field(() => String, { nullable: true })
  lastName?: Maybe<string>;

  @Field(() => String)
  email!: string;

  @Field(() => UserRole)
  role!: UserRole;

  @Field(() => UserStatus)
  status!: UserStatus;
}

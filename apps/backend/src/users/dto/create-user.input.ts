import { InputType, Field } from '@nestjs/graphql';
import { Maybe } from '@user-handling/common';
import { IsEmail, IsOptional, IsString } from 'class-validator';

@InputType()
export class CreateUserInput {
  @IsString()
  @IsOptional()
  @Field(() => String, { nullable: true })
  firstName?: Maybe<string>;

  @IsString()
  @IsOptional()
  @Field(() => String, { nullable: true })
  lastName?: Maybe<string>;

  @IsEmail()
  @Field(() => String)
  email!: string;

  @IsString()
  @IsOptional()
  @Field(() => String, { nullable: true })
  password?: Maybe<string>;
}

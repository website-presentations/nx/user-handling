import { ConfigService } from '@nestjs/config';
import { Module, OnModuleInit, forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema, UserSchemaDescriptor } from './entities/user.entity';
import { ConfigSchemaType } from '../config/env-config.schema';
import { UserRole } from '@user-handling/common';
import { UserMapper } from './user.mapper';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: UserSchemaDescriptor.name, schema: UserSchema },
    ]),
    forwardRef(() => AuthModule),
  ],
  providers: [UsersResolver, UsersService, UserMapper],
  exports: [UsersResolver, UsersService, UserMapper],
})
export class UsersModule implements OnModuleInit {
  constructor(
    private readonly configService: ConfigService<ConfigSchemaType>,
    private readonly userService: UsersService
  ) {}

  async onModuleInit() {
    await this.userService.createOnInit(
      {
        email: this.configService.get('DEFAULT_ADMIN_EMAIL') ?? '',
        password: this.configService.get('DEFAULT_ADMIN_PASSWORD') ?? void 0,
      },
      UserRole.Admin
    );
  }
}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Maybe, UserRole, UserStatus } from '@user-handling/common';

export type UserDocument = UserSchemaDescriptor &
  mongoose.Document & { createdAt: number; updatedAt: number };

@Schema({ collection: 'users', timestamps: true, autoCreate: false })
export class UserSchemaDescriptor {
  @Prop({ type: mongoose.Schema.Types.String, required: true, unique: true })
  id!: string;

  @Prop({ type: mongoose.Schema.Types.String, required: false })
  firstName?: Maybe<string>;

  @Prop({ type: mongoose.Schema.Types.String, required: false })
  lastName?: Maybe<string>;

  @Prop({ type: mongoose.Schema.Types.String, required: true, unique: true })
  email!: string;

  @Prop({ type: mongoose.Schema.Types.String, required: false })
  password?: Maybe<string>;

  @Prop({
    type: mongoose.Schema.Types.String,
    enum: UserRole,
    required: true,
  })
  role!: UserRole;

  @Prop({
    type: mongoose.Schema.Types.String,
    enum: UserStatus,
    required: true,
  })
  status!: UserStatus;
}

export const UserSchema = SchemaFactory.createForClass(UserSchemaDescriptor);

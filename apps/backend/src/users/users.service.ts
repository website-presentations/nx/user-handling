import { CryptoService } from './../common/services/crypto.service';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import {
  Maybe,
  UserError,
  UserRole,
  UserStatus,
  removeVoidProps,
} from '@user-handling/common';
import { ClientSession, FilterQuery, Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { UserSchemaDescriptor, UserDocument } from './entities/user.entity';
import { UserProp } from './user.types';
import { UserDomain } from './domain/user.domain';
import { UserMapper } from './user.mapper';

@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);

  @InjectModel(UserSchemaDescriptor.name)
  private readonly User!: Model<UserDocument>;

  constructor(
    private readonly cryptoService: CryptoService,
    private readonly mapper: UserMapper
  ) {}

  async create(
    input: CreateUserInput,
    role: UserRole,
    session?: Maybe<ClientSession>
  ): Promise<UserDomain> {
    await this.ensureEmailUniqueness(input.email);

    return await new this.User(
      removeVoidProps<UserDocument>({
        ...input,
        role,
        id: this.cryptoService.uuidv4(),
        firstName: input.firstName ?? void 0,
        lastName: input.lastName ?? void 0,
        password: this.managePasswordInput(input.password),
        status: input.password ? UserStatus.Active : UserStatus.Passwordless,
      })
    )
      .save({ session })
      .then((user: UserDocument) => {
        return this.mapper.persistenceToDomain(user);
      })
      .catch((err) => {
        this.logger.error('[create] Can not create user with');
        this.logger.error({ input, role });
        this.logger.error({ err });
        throw new InternalServerErrorException(UserError.CanNotCreateUser);
      });
  }

  async createOnInit(
    input: CreateUserInput,
    role: UserRole,
    session?: Maybe<ClientSession>
  ): Promise<Maybe<UserDomain>> {
    this.logger.verbose(`Start creating user with email: [${input.email}]`);

    const isEmailUnique = await this.checkTheUniquenessOfTheProperty(
      'email',
      input.email
    );

    if (!isEmailUnique) {
      this.logger.verbose(`Skipping creation, user already exists.`);
      return void 0;
    }

    return await new this.User(
      removeVoidProps<UserDocument>({
        ...input,
        role,
        id: this.cryptoService.uuidv4(),
        firstName: input.firstName ?? void 0,
        lastName: input.firstName ?? void 0,
        password: this.managePasswordInput(input.password),
        status: input.password ? UserStatus.Active : UserStatus.Passwordless,
      })
    )
      .save({ session })
      .then((user: UserDocument) => {
        this.logger.verbose('User creation completed.');
        return this.mapper.persistenceToDomain(user);
      })
      .catch((err) => {
        this.logger.error('[createOnInit] Can not create user with');
        this.logger.error({ input, role });
        this.logger.error({ err });
        throw new InternalServerErrorException(UserError.CanNotCreateUser);
      });
  }

  async findAll(
    criteria: FilterQuery<UserDocument>,
    session?: Maybe<ClientSession>
  ) {
    const users: UserDocument[] = await this.User.find(criteria, void 0, {
      session,
    });
    return users.map((user: UserDocument) =>
      this.mapper.persistenceToDomain(user)
    );
  }

  async findOne(
    criteria: FilterQuery<UserDocument>,
    session?: Maybe<ClientSession>
  ): Promise<Maybe<UserDomain>> {
    const user: Maybe<UserDocument> = await this.User.findOne(
      criteria,
      void 0,
      { session }
    );
    return user ? this.mapper.persistenceToDomain(user) : null;
  }

  async getUserByCredentials(
    email: string,
    password: string
  ): Promise<Maybe<UserDomain>> {
    return this.findOne({
      email,
      password: this.cryptoService.hashPassword(password),
    });
  }

  async update(
    input: UpdateUserInput,
    session?: Maybe<ClientSession>
  ): Promise<boolean> {
    const { id, ...updatable } = input;
    return await this.User.updateOne({ id }, { ...updatable }, { session })
      .then(() => {
        return true;
      })
      .catch((err) => {
        this.logger.error('[update] Can not update user with');
        this.logger.error({ input });
        this.logger.error({ err });
        throw new InternalServerErrorException(UserError.CanNotCreateUser);
      });
  }

  async remove(id: string, session?: Maybe<ClientSession>): Promise<boolean> {
    return await this.User.deleteOne({ id }, { session })
      .then(() => {
        this.logger.verbose('User has been removed with');
        this.logger.verbose({ id });
        return true;
      })
      .catch((err) => {
        this.logger.error('[remove] Can not remove user with');
        this.logger.error({ id });
        this.logger.error({ err });
        throw new InternalServerErrorException(UserError.CanNotCreateUser);
      });
  }

  async ensureEmailUniqueness(email: string): Promise<void> {
    const isEmailUnique: boolean = await this.checkTheUniquenessOfTheProperty(
      'email',
      email
    );

    if (!isEmailUnique) {
      this.logger.warn(
        '[ensureEmailUniqueness] the provided email is already taken'
      );
      this.logger.warn({ email });
      throw new BadRequestException(UserError.EmailIsAlreadyTaken);
    }
  }

  async countUsersBy(
    criteria: FilterQuery<UserDocument>,
    session?: Maybe<ClientSession>
  ): Promise<number> {
    return this.User.countDocuments(criteria, { session });
  }

  // PRIVATE
  private async checkTheUniquenessOfTheProperty(
    prop: UserProp,
    value: unknown,
    session?: Maybe<ClientSession>
  ) {
    return this.User.countDocuments({ [prop]: value }, { session }).then(
      (res) => res === 0
    );
  }

  private managePasswordInput(password: Maybe<string>): Maybe<string> {
    return password ? this.cryptoService.hashPassword(password) : void 0;
  }
}

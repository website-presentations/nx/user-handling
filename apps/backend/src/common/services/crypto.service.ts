import * as crypto from 'crypto';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { v4 } from 'uuid';
import { ConfigSchemaType } from '../../config/env-config.schema';
import { slugify } from '@user-handling/common';

@Injectable()
export class CryptoService {
  logger = new Logger('CryptoService');

  constructor(
    @Inject('uuidV4Generator') private readonly uuidV4Generator: typeof v4,
    private configService: ConfigService<ConfigSchemaType>
  ) {}

  private readonly SALT = this.configService.get('HASH_SALT');

  hashPassword(pw: string): string {
    return this.hasher(pw);
  }

  private hasher(str: string): string {
    return crypto
      .createHash('sha256')
      .update(this.SALT + str)
      .digest('hex');
  }

  quickHash(data: string): string {
    return crypto.createHash('sha1').update(data).digest('base64');
  }

  urlFriendlyQuickHash(data: string): string {
    return slugify(this.quickHash(data).replace('/', '_'));
  }

  uuidv4(): string {
    return this.uuidV4Generator();
  }
}

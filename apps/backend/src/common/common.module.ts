import { Global, Module } from '@nestjs/common';
import { CryptoService } from './services/crypto.service';
import { v4 } from 'uuid';
import { registerEnumType } from '@nestjs/graphql';
import { UserRole, UserStatus } from '@user-handling/common';

@Global()
@Module({
  imports: [],
  providers: [{ provide: 'uuidV4Generator', useValue: v4 }, CryptoService],
  exports: [CryptoService],
})
export class CommonModule {
  constructor() {
    registerEnumType(UserRole, {
      name: 'UserRole',
      description: 'Represent an user role.',
    });
    registerEnumType(UserStatus, {
      name: 'UserStatus',
      description: 'Represent an user account status.',
    });
  }
}

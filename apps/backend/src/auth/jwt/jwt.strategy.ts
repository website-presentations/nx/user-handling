import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-jwt';
import { JwtPayload } from './jwt.payload.interface';
import type { Request } from 'express';
import { ConfigSchemaType } from '../../config/env-config.schema';
import { ICacheService } from '../../cache/cache-service.interface';
import { CacheServiceSymbol } from '../../cache/tokens';
import { UsersService } from '../../users/users.service';
import { UserMapper } from '../../users/user.mapper';
import { UserDto } from '../../users/dto/user.dto';
import { UserDomain } from '../../users/domain/user.domain';
import { AuthError, Maybe } from '@user-handling/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(JwtStrategy.name);

  constructor(
    private userService: UsersService,
    private readonly mapper: UserMapper,
    private configService: ConfigService<ConfigSchemaType>,
    @Inject(CacheServiceSymbol)
    private readonly cacheService: ICacheService
  ) {
    super({
      secretOrKey: configService.get('JWT_SECRET'),
      jwtFromRequest: (req: Request) => {
        if (!req || !req.cookies) {
          this.logger.debug('Did not find cookies in request.');
          return null;
        }

        const jwtToken = req.cookies['access_token'];

        const tokenIsInCache = this.cacheService.getWithoutTouch(jwtToken);

        if (!tokenIsInCache) {
          this.logger.debug('Did not find this token on whitelist.');
          this.logger.debug(JSON.stringify({ jwtToken }, null, 2));
          return null;
        }

        return jwtToken;
      },
    });
  }
  async validate(payload: JwtPayload): Promise<UserDto> {
    const { email } = payload;
    const user: Maybe<UserDomain> = await this.userService.findOne({ email });

    if (!user) {
      this.logger.warn('[ validate ] Can not determine user with');
      this.logger.warn({ email });
      throw new BadRequestException(AuthError.InvalidToken);
    }

    return this.mapper.domainToDto(user);
  }
}

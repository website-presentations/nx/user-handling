import { UserRole, UserStatus } from '@user-handling/common';

export interface JwtPayload {
  email: string;
  role: UserRole;
  status: UserStatus;
}

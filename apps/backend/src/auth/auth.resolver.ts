import { InternalServerErrorException, Logger } from '@nestjs/common';
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import type { Response, Request } from 'express';
import { AuthManager } from '../auth/auth.manager';
import { CurrentUser } from './decorators/current-user.decorator';
import { Public } from './decorators/public.decorator';
import { UserDto } from '../users/dto/user.dto';
import { UserDomain } from '../users/domain/user.domain';
import { UserError } from '@user-handling/common';

@Resolver('Auth')
export class AuthResolver {
  private readonly logger = new Logger('AuthResolver');

  constructor(private authManager: AuthManager) {}

  @Query(() => UserDto)
  async whoAmI(@CurrentUser() user: UserDto): Promise<UserDto> {
    return user;
  }

  @Mutation(() => Boolean)
  @Public()
  async magicRegistration(
    @Context('res') response: Response,
    @Args('email') email: string
  ): Promise<boolean> {
    const user: UserDomain = await this.authManager.registration(email);
    const token: string = await this.authManager.loginWithMagicLink(user.email);

    this.authManager.attachJwtAsCookie(response, token);
    return true;
  }

  @Mutation(() => Boolean)
  @Public()
  async registration(
    @Context('res') response: Response,
    @Args('email') email: string,
    @Args('password') password: string
  ) {
    const user: UserDomain = await this.authManager.registration(
      email,
      password
    );

    if (!user) {
      this.logger.warn('[ registration ] Can not create user, with');
      this.logger.warn({ email });
      throw new InternalServerErrorException(UserError.CanNotCreateUser);
    }

    const jwtToken: string = await this.authManager.userLogin(email, password);
    this.authManager.attachJwtAsCookie(response, jwtToken);

    return true;
  }

  @Mutation(() => Boolean)
  @Public()
  async adminLogin(
    @Context('res') response: Response,
    @Args('email') email: string,
    @Args('password') password: string
  ): Promise<boolean> {
    const jwtToken: string = await this.authManager.adminLogin(email, password);
    this.authManager.attachJwtAsCookie(response, jwtToken);
    return true;
  }

  @Mutation(() => Boolean)
  @Public()
  async login(
    @Context('res') response: Response,
    @Args('email') email: string,
    @Args('password') password: string
  ): Promise<boolean> {
    const jwtToken: string = await this.authManager.userLogin(email, password);
    this.authManager.attachJwtAsCookie(response, jwtToken);
    return true;
  }

  @Mutation(() => Boolean)
  logout(
    @Context('res') response: Response,
    @Context('req') request: Request
  ): Promise<boolean> {
    this.authManager.trashJwtTokenCookie(response);
    return this.authManager.logout(request.cookies['access_token']);
  }
}

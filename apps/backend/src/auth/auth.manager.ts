import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ICacheService } from '../cache/cache-service.interface';
import { CacheServiceSymbol } from '../cache/tokens';
import { ConfigSchemaType } from '../config/env-config.schema';
import { JwtPayload } from './jwt/jwt.payload.interface';
import type { Response } from 'express';
import type { CookieOptions } from 'express-serve-static-core';
import {
  AuthError,
  CallerFunctionName,
  Maybe,
  UserRole,
  UserStatus,
} from '@user-handling/common';
import { UsersService } from '../users/users.service';
import { UserDto } from '../users/dto/user.dto';
import { UserDomain } from '../users/domain/user.domain';

type CookieSettings = { cookieKey: string; cookieDetails: CookieOptions };

@Injectable()
export class AuthManager {
  private readonly logger = new Logger(AuthManager.name);
  private readonly shouldUseSecureCookie: boolean;
  private readonly tokenTTL = 3600; //1 hour

  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UsersService,
    @Inject(CacheServiceSymbol)
    private readonly cacheService: ICacheService,
    private readonly configService: ConfigService<ConfigSchemaType>
  ) {
    this.shouldUseSecureCookie =
      configService.get<string>('ENVIRONMENT') === 'production';
  }

  async registration(email: string, password?: Maybe<string>) {
    return this.userService.create(
      { email, password: password ?? void 0 },
      UserRole.User
    );
  }

  async userLogin(email: string, password: string): Promise<string> {
    const user: UserDto = await this.validateLogin(email, password);

    this.checkForbiddenStatus(user, 'userLogin');

    return this.login(user);
  }

  async adminLogin(email: string, password: string): Promise<string> {
    const user: UserDto = await this.validateLogin(email, password);

    if (user.role !== UserRole.Admin) {
      this.logger.warn(
        '[ adminLogin ] Invalid role login attempt in the admin interface'
      );
      this.logger.warn({ email });
      throw new BadRequestException(AuthError.InvalidCredentials);
    }

    this.checkForbiddenStatus(user, 'adminLogin');

    return this.login(user);
  }

  async loginWithMagicLink(email: string): Promise<string> {
    const user: Maybe<UserDto> = await this.userService.findOne({
      email,
    });

    if (!user) {
      this.logger.warn('[ loginWithMagicLink ] Can not determine user with');
      this.logger.warn({ email });
      throw new BadRequestException(AuthError.InvalidCredentials);
    }

    this.checkForbiddenStatus(user, 'loginWithMagicLink');

    return this.login(user);
  }

  async logout(token: string): Promise<boolean> {
    await this.cacheService.expire(token);
    return true;
  }

  async validateToken(
    token: string
  ): Promise<{ email?: string; userRole: string }> {
    this.checkTheTokenIsValidString(token);

    this.checkTheTokenIsWhitelisted(token, 'validateToken');

    let verifyResult;
    try {
      verifyResult = await this.jwtService.verify(token, {
        secret: this.configService.get('JWT_SECRET'),
      });
    } catch (err) {
      this.logger.error('[ validateToken ] Invalid token provided');
      this.logger.error({ token });
      this.logger.error({ err });
      throw new BadRequestException(AuthError.InvalidToken);
    }

    const { email, role } = verifyResult;

    if (!email) {
      this.logger.warn(
        '[ validateToken ] The token does not contain email address!'
      );
      throw new BadRequestException(AuthError.InvalidToken);
    }

    return {
      email,
      userRole: role,
    };
  }

  async regenerateTokenIfNeeded(token: string): Promise<string> {
    this.checkTheTokenIsValidString(token);

    this.checkTheTokenIsWhitelisted(token, 'regenerateTokenIfNeeded');

    const tokenCreationTimestamp: Maybe<number> =
      await this.getTokenCreationTimestamp(token);

    if (!tokenCreationTimestamp) {
      this.logger.warn(
        `[ regenerateTokenIfNeeded ] Token is not in cache, with:`
      );
      this.logger.warn({ tokenCreationTimestamp });
      throw new BadRequestException(AuthError.InvalidToken);
    }

    if (Math.floor(Date.now() / 1000) - tokenCreationTimestamp < 900) {
      return token;
    }

    await this.cacheService.expire(token);

    let verifyResult;
    try {
      verifyResult = await this.jwtService.verify(token, {
        secret: this.configService.get('JWT_SECRET'),
      });
    } catch (err) {
      this.logger.error('[ regenerateTokenIfNeeded ] Invalid token provided');
      this.logger.error({ token });
      this.logger.error({ err });
      throw new BadRequestException(AuthError.InvalidToken);
    }

    const { email, role } = verifyResult;

    const user: Maybe<UserDto> = await this.userService.findOne({
      email,
    });

    if (!user) {
      this.logger.error('[ regenerateTokenIfNeeded ] Invalid token provided');
      this.logger.error({ email });
      throw new BadRequestException(AuthError.InvalidToken);
    }

    const payload: JwtPayload = { email, role, status: user.status };
    const newToken: string = this.jwtService.sign(payload, {
      expiresIn: this.tokenTTL,
    });

    this.cacheService.forceSet(newToken, {
      element: Math.floor(Date.now() / 1000),
      expiresAfter: this.tokenTTL,
    });

    return newToken;
  }

  constructHttpSecureCookieForJwtDetails(): CookieSettings {
    return {
      cookieKey: 'access_token',
      cookieDetails: {
        httpOnly: true,
        // domain: 'localhost',
        sameSite: 'lax',
        secure: this.shouldUseSecureCookie,
        // signed: true,
        expires: new Date(Date.now() + this.tokenTTL * 1000),
      },
    };
  }

  attachJwtAsCookie(response: Response, jwtToken: string): void {
    const { cookieKey, cookieDetails } =
      this.constructHttpSecureCookieForJwtDetails();
    response.cookie(cookieKey, jwtToken, { ...cookieDetails });
  }

  trashJwtTokenCookie(response: Response): void {
    const { cookieKey, cookieDetails } =
      this.constructHttpSecureCookieForJwtDetails();
    response.cookie(cookieKey, '', {
      ...cookieDetails,
      sameSite: 'none',
      expires: new Date(Date.now() - 1),
    });
  }

  isTokenInWhiteList(token: string): boolean {
    return !!this.cacheService.getWithoutTouch(token);
  }

  /* -------------------------- PRIVATE -------------------------- */

  private async validateLogin(
    email: string,
    password: string
  ): Promise<UserDto> {
    const user: Maybe<UserDomain> = await this.userService.getUserByCredentials(
      email,
      password
    );

    if (!user) {
      this.logger.warn(`[ validateLogin ] Login with invalid credentials`);
      this.logger.warn({ email });
      throw new BadRequestException(AuthError.InvalidCredentials);
    }

    return user;
  }

  private async getTokenCreationTimestamp(
    token: string
  ): Promise<Maybe<number>> {
    return this.cacheService.getWithoutTouch<number>(token);
  }

  private async login(user: UserDto): Promise<string> {
    const { email, role, status } = user;

    const payload: JwtPayload = { email, role, status };
    const token: string = this.jwtService.sign(payload, {
      expiresIn: this.tokenTTL,
    });

    this.cacheService.forceSet(token, {
      element: Math.floor(Date.now() / 1000),
      expiresAfter: this.tokenTTL,
    });

    return token;
  }

  private checkForbiddenStatus(
    user: UserDto,
    caller: CallerFunctionName
  ): void {
    if (user.status !== UserStatus.Forbidden) {
      return void 0;
    }

    this.logger.warn(`[ ${caller} ] A forbidden user tried to log in`);
    this.logger.warn({ email: user.email });
    throw new BadRequestException(AuthError.ForbiddenAccount);
  }

  private checkTheTokenIsValidString(token: string): void {
    if (token) {
      return void 0;
    }
    this.logger.warn('[ validateToken ] Token not provided!');
    this.logger.warn({ token });
    throw new BadRequestException(AuthError.InvalidToken);
  }

  private checkTheTokenIsWhitelisted(
    token: string,
    caller: CallerFunctionName
  ): void {
    const isTokenWhitelisted: boolean = this.isTokenInWhiteList(token);

    if (!isTokenWhitelisted) {
      this.logger.warn(`[ ${caller} ] Token is not in cache, with:`);
      this.logger.warn(JSON.stringify({ token, isTokenWhitelisted }, null, 2));
      throw new BadRequestException(AuthError.InvalidToken);
    }
  }
}

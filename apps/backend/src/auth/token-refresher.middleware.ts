import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { AuthManager } from './auth.manager';

@Injectable()
export class TokenRefresherMiddleware implements NestMiddleware {
  private readonly logger = new Logger(TokenRefresherMiddleware.name);
  constructor(private readonly authManager: AuthManager) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const token: string = req.cookies.access_token;

    if (!token) {
      next();
      return;
    }

    const isTokenWhitelisted = this.authManager.isTokenInWhiteList(token);

    if (!isTokenWhitelisted) {
      this.logger.error(`Can not determine token from the white list`);
      this.logger.error(JSON.stringify({ token, isTokenWhitelisted }, null, 2));
      this.authManager.trashJwtTokenCookie(res);
      next();
      return;
    }

    const newToken = await this.authManager.regenerateTokenIfNeeded(token);

    this.authManager.attachJwtAsCookie(res, newToken);
    next();
  }
}

import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
import { AuthManager } from '../auth.manager';
import { UserRole } from '@user-handling/common';

@Injectable()
export class JWTAdminGuard extends AuthGuard('jwt') implements CanActivate {
  private readonly logger = new Logger('JWTAdminGuard');

  constructor(private readonly authManager: AuthManager) {
    super();
  }

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }

  async canActivate(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;

    const token = req.cookies.access_token;

    if (!token) {
      this.logger.error('Token not provided.');
      return false;
    }

    const { userRole } = await this.authManager.validateToken(token);

    return userRole === UserRole.Admin;
  }
}

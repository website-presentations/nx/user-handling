import { Module, forwardRef } from '@nestjs/common';
import { CacheModule } from '../cache/cache.module';
import { AuthManager } from './auth.manager';
import { JwtModule } from '@nestjs/jwt';
import { MailService } from '../mail/mail.service';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtStrategy } from './jwt/jwt.strategy';
import { ConfigSchemaType } from '../config/env-config.schema';
import { AuthResolver } from './auth.resolver';
import { JWTAuthGuard } from './guards/auth.guard';
import { UsersModule } from '../users/users.module';
import { JWTAdminGuard } from './guards/admin.guard';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService<ConfigSchemaType>) => ({
        secret: config.get<string>('JWT_SECRET'),
      }),
    }),
    CacheModule,
    forwardRef(() => UsersModule),
  ],
  providers: [
    AuthManager,
    AuthResolver,
    JWTAdminGuard,
    JWTAuthGuard,
    MailService,
    JwtStrategy,
  ],
  controllers: [],
  exports: [
    AuthManager,
    JWTAuthGuard,
    JWTAdminGuard,
    AuthResolver,
    JwtStrategy,
  ],
})
export class AuthModule {}

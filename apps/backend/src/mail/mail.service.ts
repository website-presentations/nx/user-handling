import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as pug from 'pug';
import * as path from 'path';
import { ConfigSchemaType } from '../config/env-config.schema';

@Injectable()
export class MailService {
  private logger = new Logger('MailService');
  constructor(private configService: ConfigService<ConfigSchemaType>) {}
  Mailjet = require('node-mailjet');

  mailService = this.Mailjet.apiConnect(
    this.configService.get('MAILJET_API_KEY'),
    this.configService.get('MAILJET_API_SECRET')
  );

  mailServiceTest() {
    this.mailService
      .post('send', { version: 'v3.1' })
      .request({
        Messages: [
          {
            From: {
              Email: this.configService.get('MAIL_DEFAULT_SENDER'),
              Name: this.configService.get('MAIL_DEFAULT_SENDER_NAME'),
            },
            To: [
              {
                Email: this.configService.get('MAIL_DEFAULT_SENDER'),
                Name: 'You',
              },
            ],
            Subject: 'My first Mailjet Email!',
            TextPart: 'Greetings from Mailjet!',
            HTMLPart:
              '<h3>Dear passenger 1, welcome to <a href="https://www.mailjet.com/">Mailjet</a>!</h3><br />May the delivery force be with you!',
          },
        ],
      })
      .then((result: any) => {
        this.logger.log(result.body);
      })
      .catch((err: any) => {
        this.logger.log(err.statusCode);
      });
  }

  setPasswordLink(email: string, setPasswordLink: string) {
    const pathToTemplate = path.join(
      __dirname,
      'mail/templates/setPasswordLink.pug'
    );
    const compiledFunction = pug.compileFile(pathToTemplate);
    this.mailService.post('send', { version: 'v3.1' }).request({
      Messages: [
        {
          From: {
            Email: this.configService.get('MAIL_DEFAULT_SENDER'),
            Name: this.configService.get('MAIL_DEFAULT_SENDER_NAME'),
          },
          To: [
            {
              Email: email,
              Name: 'You',
            },
          ],
          Subject: 'SysCops ERP jelszó beállítása',
          TextPart: 'SysCops ERP jelszó beállítása',
          HTMLPart: compiledFunction({ link: setPasswordLink }),
        },
      ],
    });
  }
}

import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import {
  ConfigSchemaType,
  envConfigValidationSchema,
} from '../config/env-config.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { CommonModule } from '../common/common.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriverConfig, ApolloDriver } from '@nestjs/apollo';
import * as path from 'path';
import { UsersModule } from '../users/users.module';
import { CacheModule } from '../cache/cache.module';
import { TokenRefresherMiddleware } from '../auth/token-refresher.middleware';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: false,
      validationSchema: envConfigValidationSchema,
      validationOptions: {
        abortEarly: true,
      },
    }),
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService<ConfigSchemaType>) => {
        return {
          uri: config.get<string>('MONGO_URI'),
          connectTimeoutMS: 3000,
          serverSelectionTimeoutMS: 1000,
          socketTimeoutMS: 5000,
          dbName: config.get<string>('DB_NAME'),
        };
      },
    }),
    GraphQLModule.forRootAsync<ApolloDriverConfig>({
      inject: [ConfigService],
      driver: ApolloDriver,
      useFactory: (config: ConfigService<ConfigSchemaType>) => {
        const isDev = config.get<string>('ENVIRONMENT') === 'development';
        return {
          debug: isDev,
          playground: isDev,
          autoSchemaFile: path.join(
            process.cwd(),
            'backend-api-schema/schema.gql'
          ),
          sortSchema: true,
          cors: true,
          context: (context: any) => context,
        };
      },
    }),
    CommonModule,
    CacheModule,
    UsersModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(TokenRefresherMiddleware).forRoutes('*');
  }
}

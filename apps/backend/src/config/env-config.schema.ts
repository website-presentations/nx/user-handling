import Joi = require('joi');

const configSchema = {
  ENVIRONMENT: Joi.string().valid('production', 'development').required(),
  DB_NAME: Joi.string().required(),
  MONGO_URI: Joi.string().required(),
  SERVER_PORT: Joi.number().required(),
  JWT_SECRET: Joi.string().required(),
  CORS_URL_WHITELIST: Joi.string().required(),
  BACKEND_BASE_URL: Joi.string().required(),
  IS_RUNNING_ON_PROD_SERVER: Joi.boolean().required(),
  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.number().required(),
  MAILJET_API_KEY: Joi.string().required(),
  MAILJET_API_SECRET: Joi.string().required(),
  MAIL_DEFAULT_SENDER: Joi.string().required(),
  MAIL_DEFAULT_SENDER_NAME: Joi.string().required(),
  HASH_SALT: Joi.string().required(),
  REG_EXPIRED_TIME: Joi.number().required(),
  REG_EXPIRED_MAIL_AFTER: Joi.number().required(),
  DEFAULT_ADMIN_USERNAME: Joi.string().required(),
  DEFAULT_ADMIN_EMAIL: Joi.string().required(),
  DEFAULT_ADMIN_PASSWORD: Joi.string(),
  HTTP_TIMEOUT: Joi.number(),
  HTTP_MAX_REDIRECTS: Joi.number(),
};

export const envConfigValidationSchema = Joi.object(configSchema);

export type ConfigKeys = keyof typeof configSchema;

export type ConfigSchemaType = typeof configSchema;

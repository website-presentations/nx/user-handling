import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CacheServiceSymbol } from './tokens';
import { CacheServiceRedisAdapter } from './cache-service-redis-adapter.service';
import { ConfigSchemaType } from '../config/env-config.schema';

@Module({
  imports: [],
  providers: [
    {
      provide: CacheServiceSymbol,
      useFactory: (config: ConfigService<ConfigSchemaType>) => {
        return new CacheServiceRedisAdapter({
          port: config.get<number>('REDIS_PORT'),
          host: config.get<string>('REDIS_HOST'),
        });
      },
      inject: [ConfigService],
    },
    { provide: 'cacheService', useValue: CacheServiceRedisAdapter },
  ],
  exports: [CacheServiceSymbol],
})
export class CacheModule {}

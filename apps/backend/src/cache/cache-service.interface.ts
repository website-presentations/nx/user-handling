import { Maybe } from '@user-handling/common';

export type CacheableResult<T> = { element: T; expiresAfter: number };
export type CacheElementFactory<T> = () =>
  | Promise<CacheableResult<T>>
  | CacheableResult<T>;

export interface ICacheService {
  getWithoutTouch<T>(key: string): Promise<Maybe<T>> | Maybe<T>;
  get<T>(key: string, factory: CacheElementFactory<T>): Promise<T> | T;
  forceSet<T>(key: string, element: CacheableResult<T>): Promise<T> | T;
  expire<T>(key: string): Promise<Maybe<T>> | Maybe<T>;
}

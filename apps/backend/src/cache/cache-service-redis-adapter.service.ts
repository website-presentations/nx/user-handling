import type {
  CacheableResult,
  CacheElementFactory,
  ICacheService,
} from './cache-service.interface';

import Redis, { RedisOptions } from 'ioredis';

import { Injectable } from '@nestjs/common';
import { currentTimestamp, Maybe } from '@user-handling/common';

export type { RedisOptions };
@Injectable()
export class CacheServiceRedisAdapter implements ICacheService {
  private readonly redis: Redis;

  constructor(options: RedisOptions) {
    this.redis = new Redis(options);
  }

  async expire<T>(key: string): Promise<Maybe<T>> {
    const elem = await this.getWithoutTouch<T>(key);
    await this.redis.expireat(key, currentTimestamp() - 1);

    return elem;
  }

  async forceSet<T>(key: string, element: CacheableResult<T>): Promise<T> {
    const { element: storable, expiresAfter } = element;

    const res = await this.redis.set(key, JSON.stringify(storable));
    if (res !== 'OK') throw new Error('Unable to cache element!');
    await this.redis.expireat(key, currentTimestamp() + expiresAfter);

    return storable;
  }

  async get<T>(key: string, factory: CacheElementFactory<T>): Promise<T> {
    const elem = await this.getWithoutTouch<T>(key);
    if (elem) return elem;

    return this.forceSet(key, await factory());
  }

  async getWithoutTouch<T>(key: string): Promise<Maybe<T>> {
    const elem = await this.redis.get(key);
    return elem ? JSON.parse(elem) : null;
  }
}

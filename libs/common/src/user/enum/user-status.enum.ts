export enum UserStatus {
  Forbidden = 'forbidden',
  Passwordless = 'passwordless',
  Active = 'active',
}

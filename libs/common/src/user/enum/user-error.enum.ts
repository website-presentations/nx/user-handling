export enum UserError {
  EmailIsAlreadyTaken = 'EMAIL_IS_ALREADY_TAKEN',
  UserDoesNotExists = 'USER_DOES_NOT_EXISTS',
  CanNotCreateUser = 'CAN_NOT_CREATE_USER',
  CanNotUpdateUser = 'CAN_NOT_UPDATE_USER',
  CanNotRemoveUser = 'CAN_NOT_REMOVE_USER',
}

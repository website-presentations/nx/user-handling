export type Maybe<T> = T | null | undefined;
export type CallerFunctionName = string;
export type NumericRange = { min: number; max: number };

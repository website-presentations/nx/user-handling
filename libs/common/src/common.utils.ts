import { NumericRange } from './common.types';

export const removeVoidProps = <T>(obj: Partial<T>): T => {
  for (const key in obj) {
    if (obj[key] !== void 0) {
      continue;
    }

    delete obj[key];
  }
  return obj as T;
};

export const currentTimestamp = () => Math.floor(Date.now() / 1000);

export const slugify = (string: string): string => {
  const a =
    'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;';
  const b =
    'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------';
  const p = new RegExp(a.split('').join('|'), 'g');

  return string
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w-]+/g, '') // Remove all non-word characters
    .replace(/--+/g, '-') // Replace multiple - with single -
    .replace(/'+/g, '-') // Replace '
    .replace(/"+/g, '-') // Replace "
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, ''); // Trim - from end of text
};

export const getRandomInt = (range: NumericRange): number => {
  const min = Math.ceil(range.min);
  const max = Math.floor(range.max);
  return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
};

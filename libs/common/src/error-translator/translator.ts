import { Lang } from '../common.enum';
import * as errors from './errors.json';

export type ErrorCodeKey = keyof typeof errors;

export const translateErrorCodeToMessage = (
  code: ErrorCodeKey,
  lang = Lang.En
): string => {
  const errorDetails = errors[code];
  if (!errorDetails) return code;

  return errorDetails.message[lang];
};

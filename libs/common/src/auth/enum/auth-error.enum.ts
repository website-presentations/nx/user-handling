export enum AuthError {
  InvalidToken = 'INVALID_TOKEN',
  ForbiddenAccount = 'FORBIDDEN_ACCOUNT',
  InvalidCredentials = 'INVALID_CREDENTIALS',
}
